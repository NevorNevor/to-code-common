<?php

namespace ToCode\Common\Json;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionObject;

class Mapper
{
    public function parse(array $array, String $class){
        return $this->parseArray($array, $class);
    }

    public function parseArray(array $array, String $class){
        $object = new $class();
        $reflect = new ReflectionClass($class);
        $fields = $reflect->getProperties();
        foreach ($array as $key => $value) {
            $exists = false;
            foreach ($fields as $field) {
                if (!$exists) {
                    $exists = $field->getName() === $key;
                }
            }
            if (!$exists) {
                throw new InvalidArgumentException("Class $class have no $key field, parsing failed");
            }
            $reflectionProperty = $reflect->getProperty($key);
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($object, $value);
        }
        return $object;
    }

    public function parseObject($object, String $class){
        $mappedObject = new $class();
        $reflectClass = new ReflectionClass($class);
        $reflectObject = new ReflectionObject($object);
        $objectFields = $reflectObject->getProperties();
        $classFields = $reflectClass->getProperties();
        foreach ($objectFields as $objectField) {
            $exists = false;
            foreach ($classFields as $classField) {
                if (!$exists) {
                    $exists = $classField->getName() === $objectField->getName();
                }
            }
            if (!$exists) {
                throw new InvalidArgumentException("Class $class have no $objectField field, parsing failed");
            }
            $reflectionClassProperty = $reflectClass->getProperty($objectField->getName());
            $reflectionObjectProperty = $reflectObject->getProperty($objectField->getName());
            $reflectionClassProperty->setAccessible(true);
            try {
                $value = $reflectionObjectProperty->getValue($object);
            }catch (ReflectionException $ex){
                throw new InvalidArgumentException("Can't read $object field '$objectField' value");
            }
            $reflectionClassProperty->setValue($mappedObject, $value);
        }
        return $mappedObject;
    }
}