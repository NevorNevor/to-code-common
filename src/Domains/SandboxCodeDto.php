<?php

namespace ToCode\Common\Domains;

class SandboxCodeDto implements IDto
{
    /**
     * @var String
     */
    private $code;

    /**
     * PHPSandboxCodeDto constructor.
     * @param string $code
     */
    public function __construct(string $code = null)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    function jsonSerialize()
    {
        $code = $this->code;
        return compact('code');
    }

}