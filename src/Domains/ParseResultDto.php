<?php

namespace ToCode\Common\Domains;

class ParseResultDto implements IDto
{
    /**
     * @var string
     */
    private $result;
    /**
     * @var bool
     */
    private $error;

    /**
     * ParseResultDto constructor.
     * @param string $result
     * @param bool $isError
     */
    public function __construct(string $result = null, bool $isError = false)
    {
        $this->result = $result;
        $this->error = $isError;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult(string $result)
    {
        $this->result = $result;
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error;
    }

    /**
     * @param bool $error
     */
    public function setError(bool $error)
    {
        $this->error = $error;
    }

    function jsonSerialize()
    {
        $result = $this->result;
        $error = $this->error;
        return compact('result', 'error');
    }

}