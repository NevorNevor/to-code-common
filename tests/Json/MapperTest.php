<?php

namespace Tests\Json;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use stdClass;
use ToCode\Common\Domains\ParseResultDto;
use ToCode\Common\Domains\SandboxCodeDto;
use ToCode\Common\Json\Mapper;

class MapperTest extends TestCase
{
    /**
     * @var Mapper
     */
    private $mapper;

    public function testParseArray()
    {
        $array = [
            'code' => 'result'
        ];
        $result = $this->mapper->parseArray($array, SandboxCodeDto::class);
        $this->assertTrue($result instanceof SandboxCodeDto);
        $this->assertEquals('result',$result->getCode());
    }

    public function testParseArrayFailed()
    {
        $array = [
            'code' => 'result'
        ];
        $this->expectException(InvalidArgumentException::class);
        $this->mapper->parseArray($array, ParseResultDto::class);
    }

    public function testParseObject()
    {
        $object = new stdClass();
        $object->code = 'result';
        $result = $this->mapper->parseObject($object, SandboxCodeDto::class);
        $this->assertTrue($result instanceof SandboxCodeDto);
        $this->assertEquals('result',$result->getCode());
    }

    public function testParseObjectFailed()
    {
        $object = new stdClass();
        $object->incorrect = 'result';
        $this->expectException(InvalidArgumentException::class);
        $this->mapper->parseObject($object, SandboxCodeDto::class);
    }

    protected function setUp()
    {
        $this->mapper = new Mapper();
        parent::setUp();
    }

}